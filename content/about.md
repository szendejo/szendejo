+++
title = "About@szendejo.me $"
description = "Who is Stephanie Zendejo?"
date = "2019-02-28"
aliases = ["about-us", "about-stephanie", "contact"]
author = "Stephanie Zendejo"
+++

Hey there. My name is Stephanie Zendejo. I'm a software developer by day and an avid reality tv show watcher by night. I'm currently working 
at an education technology company, where I write in C#, MySql, HTML/CSS, Razor, JS, AngularJS and utilize the .NET framework.
This website was coded by me. It utilizes the [Hugo framework](https://gohugo.io/) and is deployed by [GitLab](https://gitlab.com).